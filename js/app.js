/*Declarar Variables*/

const btnCalcular = document.getElementById("btnCalcular");
const btnLimpiar = document.getElementById("btnLimpiar");

btnCalcular.addEventListener("click", function(){
    let valorAuto = document.getElementById("valorAutos").value;
    let pInicial = document.getElementById("porcentaje").value;
    let Plazos = document.getElementById("plazos").value;
    let parrafo = document.getElementById("pa");

    // Hacer los Calculos

    let pagoInicial = valorAuto * (pInicial/100);
    let totalFin = valorAuto - pagoInicial;
    let pagoMensual = totalFin/Plazos;


    //Mostrar los datos
    document.getElementById("pagoinicial").value = pagoInicial;
    document.getElementById("totalfin").value = totalFin;
    document.getElementById("pagomensual").value = pagoMensual;

    // Crear el registro
    let nuevoRegistro = document.createElement("div");
    nuevoRegistro.className = "registro";

    // los datos a mostrar en el registro
    nuevoRegistro.innerHTML = 
        `<p>Pago Inicial: ${pagoInicial}</p>
        <p>Total a Financiar: ${totalFin}</p>
        <p>Pago Mensual: ${pagoMensual}</p>`;

    // Agrega el registro a la sección de registros
    let registros = document.getElementById("registros");
    registros.appendChild(nuevoRegistro);

});

function arribaMouse(){
    parrafo = document.getElementById("pa");
    parrafo.style.color = "#FF00FF";
    parrafo.style.fontSize= "25px";
    parrafo.style.textAlign = "justify";
}

function salirMouse(){
    parrafo = document.getElementById("pa");
    parrafo.style.color = "red";
    parrafo.style.fontSize= "17px";
    parrafo.style.textAlign = "left";
}

function limpiar(){
    document.getElementById("valorAutos").value = "" ;
    document.getElementById("porcentaje").value = "" ;
    document.getElementById("plazos").value = "12" ;
    document.getElementById("pagoinicial").value = "" ;
    document.getElementById("totalfin").value = "" ;
    document.getElementById("pagomensual").value = "" ;
    parrafo.innerHTML = "";
}

/* Manejo de Arrays */

//Declaracion de array con elemntos enteros 
let arreglo = [4,89,30,10,34,89,10,5,800,28];

//diseñar una funcion que recibe como argumento un arreglo
// de enteros e imprime cada elemento y el tamaño del arreglo

function  mostrarArray(arreglo){
let tamaño = arreglo.length ;

for(let con=0 ; con < arreglo.length; ++ con){
    console.log(con + ":" + arreglo[con]);

}
console.log("tamaño :" + tamaño);

}

//funcion mostrar el promedio de los elementos
//de array

function getAvg(arreglo){
    let res = 0;
    for(let i = 0; i < arreflo.length; i++){
        res += arreglo[i];
    }
    return (res/arreglo.length);
}
console.log (getAvg(arreglo));
console.log (getAvg(mostrarArray));
console.log(arreglo);
console.log(mostrarMayor(arreglo));
console.log(ramdonVar(arreglo));
console.log(mostrarMenor(arreglo));
console.log (esSimetrico(arreglo));

//funcion para mostrar los valores pares del arreglo 
const mostrarPares = (arreglo) => {
    let pares = [];
    for(let i = 0; i < arreglo.length; i ++){
        if(arreglo[i] % 2 == 0)pares.push(arreglo[i]);
    }
    return pares;
}

//funcion para mostrar el valor mayor de los elementos de
// un arreglo
function mostrarMayor(arreglo){
    let tamaño = arreglo.length
    let mayor = 0;
    let posicion =0;

    for(let con=0;con<tamaño;con++){
        if(arreglo[con]>mayor){
            posicion=con
        }
    }
}
console.log("El elemnto mayor: "+mayor+" y esta es la posicion:")

//funcion para llenar con valores aleatorios un arreglo
function ramdonVar(arreglo) {
    let tamaño = arreglo.length;
    for(let i = 0; i < tamaño; i++){
        arreglo[i] = (Math.random() * 10).toFixed(2);
    }
    console.log(i + ":" + arreglo[i]);
}

//funcion que muestra el valor menor y la posicion del arreglo
function mostrarMenor(arreglo) {
    let menor = arreglo[0]; // Inicializamos menor con el primer elemento del arreglo
    let posicion = 0;
  
    for (let con = 1; con < arreglo.length; con++) {
      if (arreglo[con] < menor) {
        menor = arreglo[con];
        posicion = con;
      }
    }
    console.log("El elemento menor es: " + menor + " y esta es la posición: " + posicion);
  }
  
//funcion para comprobar si el generador de nuemros aleatorios
//es simetrico es decir que la diferencia entre numeros
//pares e impares no sea mayor al 20%
function esSimetrico(arreglo) {
  let pares = 0;
  let impares = 0;

  for (let i = 0; i < arreglo.length; i++) {
    if (arreglo[i] % 2 === 0) {
      pares++;
    } else {
      impares++;
    }
  }

  const total = arreglo.length;
  const porcentajePares = (pares / total) * 100;
  const porcentajeImpares = (impares / total) * 100;

  if (Math.abs(porcentajePares - porcentajeImpares) <= 20) {
    return "No Es Asimetrico";
  } else {
    return "Si Es Asimetrico";
  }
}

function llenarConNumerosAleatorios() {
    const cantidad = parseInt(document.getElementById("cantidad").value);
    const selectOpciones = document.getElementById("opciones");

    // Limpia las opciones del combo box y los resultados
    selectOpciones.innerHTML = "";
    
    let cantidadPares = 0;
    let cantidadImpares = 0;
    
    for (let i = 0; i < cantidad; i++) {
        const numero = Math.floor(Math.random() * 100) + 1; // Genera números aleatorios del 1 al 100

        // Agrega una opción al combo box con el número generado
        const opcion = document.createElement("option");
        opcion.value = numero;
        opcion.textContent = numero;
        selectOpciones.appendChild(opcion);

        if (numero % 2 === 0) {
            cantidadPares++;
        } else {
            cantidadImpares++;
        }
    }

    // Calcula el porcentaje de impares y pares
    const totalNumeros = cantidadPares + cantidadImpares;
    const porcentajeImpares = ((cantidadImpares / totalNumeros) * 100).toFixed(2);
    const porcentajePares = ((cantidadPares / totalNumeros) * 100).toFixed(2);

    // Actualiza el contenido de los elementos de resultados
    document.getElementById("porcentajeImpares").textContent = `Porcentaje de impares: ${porcentajeImpares}%`;
    document.getElementById("porcentajePares").textContent = `Porcentaje de pares: ${porcentajePares}%`;

    const esSimetrico = cantidadImpares === cantidadPares;
    const esSimetricoTexto = esSimetrico ? "Es simétrico" : "No es simétrico";
    document.getElementById("esSimetrico").textContent = esSimetricoTexto;
}

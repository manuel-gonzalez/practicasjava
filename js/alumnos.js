//Declara Objeto Alumno

let alumnos = [
    {
        "Matricula": "2021030266",
        "Nombre": "González Ramírez José Manuel",
        "Grupo": "TI - 73",
        "Carrera": "Tecnologías de la Información",
        "Foto": "/img/manuel.jpg"
    },
    {
        "Matricula": "2021030008",
        "Nombre": "Landeros Andrade María Estrella",
        "Grupo": "TI - 73",
        "Carrera": "Tecnologías de la Información",
        "Foto": "/img/estrella.jpg"
    },
    {
        "Matricula": "2020030714",
        "Nombre": "Ruiz Guerrero Axel Jovani",
        "Grupo": "TI - 73",
        "Carrera": "Tecnologías de la Información",
        "Foto": "/img/axel.jpg"
    },
    {
        "Matricula": "2021030136",
        "Nombre": "Solis Velarde Oscar Alejandro",
        "Grupo": "TI - 73",
        "Carrera": "Tecnologías de la Información",
        "Foto": "/img/oscar.jpg"
    },
    {
        "Matricula": "2021030269",
        "Nombre": "Gael Mizraim Salas Salaza",
        "Grupo": "TI - 73",
        "Carrera": "Tecnologías de la Información",
        "Foto": "/img/gael.jpg"
    },
    {
        "Matricula": "2021030101",
        "Nombre": "Plazola Arangure Yohan Alek ",
        "Grupo": "TI - 73",
        "Carrera": "Tecnologías de la Información",
        "Foto": "/img/yohan.jpg"
    },
    {
        "Matricula": "2021030262",
        "Nombre": "Qui Mora Ángel Ernesto",
        "Grupo": "TI - 73",
        "Carrera": "Tecnologías de la Información",
        "Foto": "/img/qui.jpg"
    },
    {
        "Matricula": "2020030321",
        "Nombre": "Ontiveros Govea Yair Alejandro",
        "Grupo": "TI - 73",
        "Carrera": "Tecnologías de la Información",
        "Foto": "/img/govea.jpg"
    },
    {
        "Matricula": "2019030880",
        "Nombre": "Quezada Ramos Julio Emiliano",
        "Grupo": "TI - 73",
        "Carrera": "Tecnologías de la Información",
        "Foto": "/img/julio.jpg"
    },
    {
        "Matricula": "mosuna",
        "Nombre": "Osuna Cardenas Melissa",
        "Grupo": "TI - 73 - Tutora",
        "Carrera": "Tecnologías de la Información",
        "Foto": "/img/melissa.jpg"
    }
];

// Selecciona el elemento ul donde mostrar la lista de alumnos
const ulElement = document.getElementById("alumnos-ul");

// Recorre el arreglo de alumnos y crea un elemento li para cada uno
for (let i = 0; i < alumnos.length; i++) {
    const alumnoActual = alumnos[i];
    const liElement = document.createElement("li");

    // Crea una estructura HTML con todos los campos del alumno
    liElement.innerHTML = `
        <img src="${alumnoActual.Foto}" alt="${alumnoActual.Nombre}">
        <h2>${alumnoActual.Nombre}</h2>
        <p>Matrícula: ${alumnoActual.Matricula}</p>
        <p>Grupo: ${alumnoActual.Grupo}</p>
        <p>Carrera: ${alumnoActual.Carrera}</p>
    `;

    // Agrega el li al elemento ul
    ulElement.appendChild(liElement);
}


console.log("Matricula :" + alumno.Matricula);
console.log("Nombre :" + alumno.Nombre);

alumno.Nombre = "Maria Estrella Landeros Andrade";
console.log("Nuevo Nombre :" + alumno.Nombre);

//Objetos Compuestos

let cuentaBanco = {
    "Numero" : "1001",
    "Banco" : "Banorte",
    cliente: {
        "Nombre" : "Jose Manuel",
        "FechaNac" : "2003-03-17",
        "Sexo" : "M"},
    "Saldo" : "1000"
}

console.log("Nombre :" + cuentaBanco.cliente.Nombre);
console.log("Saldo : " + cuentaBanco.Saldo);
cuentaBanco.cliente.Sexo = "F";
console.log(cuentaBanco.cliente.Sexo);

//Arreglo de Productos

let productos = [{
    "Codigo" : "1001",
    "Descripcion" : "Atun",
    "Precio" : "34",},
    {
        "Codigo" : "1002",
        "Descripcion" : "Jabon de Polvo",
        "Precio" : "23"},
    {
        "Codigo" : "1003",
        "Descripcion" : "Harina",
        "Precio" : "43"},
    {
        "Codigo" : "1004",
        "Descripcion" : "Harina",
        "Precio" : "43"},
    {    
        "Codigo" : "1005",
        "Descripcion" : "Pasta Dental",
        "Precio" : "78", 
    }
]

//Mostrar un atributo de un objeto del arreglo
console.log("La descripcion es :" + productos[0].Descripcion);

for(let i=0; i<productos.length; i++){
    console.log("Codigo: " + productos[1].Codigo);
    console.log("Descripcion: " + productos[1].Descripcion);
    console.log("Precio: " + productos[1].Precio);
}
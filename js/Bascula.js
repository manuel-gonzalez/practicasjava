const btnCalcular = document.getElementById("btnCalcular");

btnCalcular.addEventListener("click", function() {
    const altura = parseFloat(document.getElementById("altura").value);
    const peso = parseFloat(document.getElementById("peso").value);
    const edad = parseFloat(document.getElementById("edad").value);
    const sexo = document.querySelector('input[name="sexo"]:checked').value;

    if (isNaN(altura) || isNaN(peso) || isNaN(edad)) {
        alert("Ingresa valores válidos para altura, peso y edad.");
        return;
    }

    const imc = peso / Math.pow(altura, 2);
    document.getElementById("resultado").value = `${imc.toFixed(2)}`;

    const imgResultado = document.getElementById("imgResultado");
    const mensajeResultado = document.getElementById("mensajeResultado");

    if (imc < 18.5) {
        imgResultado.src = "/img/01.png";
        mensajeResultado.textContent = "IMC < 18.5: Bajo peso";
    } else if (imc >= 18.5 && imc < 25) {
        imgResultado.src = "/img/02.png";
        mensajeResultado.textContent = "IMC 18.5 - 24.9: Peso saludable";
    } else if (imc >= 25 && imc < 30) {
        imgResultado.src = "/img/03.png";
        mensajeResultado.textContent = "IMC 25 - 29.9: Sobrepeso";
    } else if (imc >= 30 && imc < 35) {
        imgResultado.src = "/img/04.png";
        mensajeResultado.textContent = "IMC 30 - 34.9: Obesidad Clase I";
    } else if (imc >= 35 && imc < 40) {
        imgResultado.src = "/img/05.png";
        mensajeResultado.textContent = "IMC 35 - 39.9: Obesidad Clase II";
    } else {
        imgResultado.src = "/img/06.png";
        mensajeResultado.textContent = "IMC ≥ 40: Obesidad Clase III";
    }

    // Cálculo de calorías diarias recomendadas
    let caloriasHombres = 0;
    let caloriasMujeres = 0;

    if (edad >= 10 && edad < 18) {
        caloriasHombres = 17.686 * peso + 658.2;
        caloriasMujeres = 13.384 * peso + 692.6;
    } else if (edad >= 18 && edad < 30) {
        caloriasHombres = 15.057 * peso + 692.2;
        caloriasMujeres = 14.818 * peso + 486.6;
    } else if (edad >= 30 && edad < 60) {
        caloriasHombres = 11.472 * peso + 873.1;
        caloriasMujeres = 8.126 * peso + 692.6;
    } else if (edad >= 60) {
        caloriasHombres = 11.711 * peso + 587.7;
        caloriasMujeres = 9.087 * peso + 658.5;
    }

    document.getElementById("caloriasHombres").textContent = `Calorías diarias recomendadas para hombres: ${caloriasHombres.toFixed(2)}`;
    document.getElementById("caloriasMujeres").textContent = `Calorías diarias recomendadas para mujeres: ${caloriasMujeres.toFixed(2)}`;
});
